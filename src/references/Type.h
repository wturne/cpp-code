#ifndef INTERVIEW_TYPE_H
#define INTERVIEW_TYPE_H

#include <iosfwd>

class Type {
public:
    Type();

    Type(const Type &type);
    Type(Type &&type) noexcept;

    void Use();
    void Reset();

private:
    static inline size_t gCount = 0;

    size_t id_;
};


#endif //INTERVIEW_TYPE_H
