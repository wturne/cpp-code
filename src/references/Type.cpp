#include "Type.h"

#include <iostream>

Type::Type() : id_(gCount++) {
    std::cout << "instance #" << id_ << " created!" << std::endl;
}

Type::Type(const Type &type) : id_(gCount++) {
    std::cout << "instance #" << type.id_ << " copied and instance #" << id_ << " created!" << std::endl;
}

Type::Type(Type &&type) noexcept : id_(std::numeric_limits<size_t>::max()) {
    std::swap(type.id_, id_);
    std::cout << "instance #" << id_ << " moved!" << std::endl;
}

void Type::Use() {
    std::cout << "instance #" << id_ << " used!" << std::endl;
}

void Type::Reset() {
    id_ = gCount++;
    std::cout << "instance #" << id_ << " reset!" << std::endl;
}
