#include <algorithm>
#include <iostream>
#include <memory>

#include "Type.h"

void mirror(std::string &left, std::string &right) {
    std::swap(left, right);
    std::reverse(left.begin(), left.end());
    std::reverse(right.begin(), right.end());
}

void demo_0() {
    std::string a_word = "cat", another_word = "dog";
    std::cout << a_word << " " << another_word << std::endl;
    mirror(a_word, another_word);
    std::cout << a_word << " " << another_word << std::endl;
}

void greet(const std::string &name) {
    std::cout << "Hello " << name << "!" << std::endl;
}

void demo_1() {
    std::string alice = "Alice";
    greet(alice);
    const std::string bob = "Bob";
    greet(bob);
    greet(std::string("Charlie"));
}

void consume(std::string &&text) {
    std::string consumed = std::move(text);
    std::cout << "stole \"" << consumed << "\"" << std::endl;
}

void demo_2() {
    consume(std::string("temporary string"));
    std::string text = "plain string";
    consume(std::move(text));
    text.clear();
    text += "appended string";
    consume(std::move(text));
}

void use_instance_of_type(Type type) {
    type.Use();
}

void demo_3() {
    Type instance;
    use_instance_of_type(instance);
    use_instance_of_type(Type());
    use_instance_of_type(std::move(instance));
}

void demo_4() {
    Type instance;
    use_instance_of_type(std::move(instance));
    instance.Use();
    instance.Reset();
    instance.Use();
}

int help() {
    std::cout << "please choose from 0-4" << std::endl;
    return 1;
}

int main(int argc, char **argv) {
    if (argc < 2)
        return help();
    std::string arg(argv[1]);
    if (!std::all_of(arg.begin(), arg.end(), [] (char c) { return std::isdigit(c); }))
        return help();
    unsigned int option = std::stoi(arg);
    switch (option) {
        case 0:
            return demo_0(), 0;
        case 1:
            return demo_1(), 0;
        case 2:
            return demo_2(), 0;
        case 3:
            return demo_3(), 0;
        case 4:
            return demo_4(), 0;
        default:
            return help();
    }
}
