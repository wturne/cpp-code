#include "Type.h"

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

void demo_0() {
    std::cout << "using three individual instances" << std::endl;
    Type instance_0, instance_1, instance_2;

    instance_0.Use();
    instance_1.Use();
    instance_2.Use();
}

void demo_1() {
    std::cout << "using each instance in array of three" << std::endl;
    Type instance[3];

    instance[0].Use();
    instance[1].Use();
    instance[2].Use();
}

void demo_2() {
    std::cout << "using three individual instances" << std::endl;
    Type *instance_0 = new Type;
    Type *instance_1 = new Type;
    Type *instance_2 = new Type;

    instance_0->Use();
    instance_1->Use();
    instance_2->Use();

    delete instance_1;
    delete instance_2;
    delete instance_0;
}

void demo_3() {
    std::cout << "using three individual instances" << std::endl;
    std::unique_ptr instance_0 = std::make_unique<Type>();
    std::unique_ptr instance_1 = std::make_unique<Type>();
    std::unique_ptr instance_2 = std::make_unique<Type>();

    instance_0->Use();
    instance_1->Use();
    instance_2->Use();

    instance_1.reset();
    instance_2.reset();
}

auto get_size() -> size_t {
    size_t size;
    std::cout << "Enter size: ";
    std::cin >> size;
    return size;
}

void demo_4() {
    size_t size = get_size();
    Type *instance = new Type[size];

    for (size_t index = 0; index < size; ++index)
        instance[index].Use();

    delete[] instance;
}

void demo_5() {
    std::vector<Type> instances(get_size());
    for (Type &instance : instances)
        instance.Use();
}

int help() {
    std::cout << "please choose from 0-5" << std::endl;
    return 1;
}

int main(int argc, char **argv) {
    if (argc < 2)
        return help();
    std::string arg(argv[1]);
    if (!std::all_of(arg.begin(), arg.end(), [] (char c) { return std::isdigit(c); }))
        return help();
    unsigned int option = std::stoi(arg);
    switch (option) {
    case 0:
        return demo_0(), 0;
    case 1:
        return demo_1(), 0;
    case 2:
        return demo_2(), 0;
    case 3:
        return demo_3(), 0;
    case 4:
        return demo_4(), 0;
    case 5:
        return demo_5(), 0;
    default:
        return help();
    }
}
