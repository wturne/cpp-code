#include "Type.h"

#include <iostream>

Type::Type() : id_(gCount++) {
    std::cout << "instance #" << id_ << " created!" << std::endl;
}

void Type::Use() {
    std::cout << "instance #" << id_ << " used!" << std::endl;
}

Type::~Type() noexcept {
    std::cout << "instance #" << id_ << " destroyed!" << std::endl;
}
